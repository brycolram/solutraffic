import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: './home/home.module#HomeModule',
    data: {title: 'Home'}
  },
  {
    path: 'todos',
    loadChildren: './todos/todos.module#TodosModule',
    data: {title: 'Todos'}
  },
  {
    path: 'detail',
    loadChildren: './detail/detail.module#DetailModule',
    data: {title: 'Detalle'}
  }, {
    path: '**',
    redirectTo: 'home'
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
