import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})

export class ApiService {
  base_url = 'https://dog.ceo/api/';
  image: String;

  constructor(private http: HttpClient) {
  }

  getImage() {
    return this.http.get(this.base_url + 'breeds/image/random');
  }

  getAll() {
    return this.http.get(this.base_url + 'breed/hound/images');
  }
}
