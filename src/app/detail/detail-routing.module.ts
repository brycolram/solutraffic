import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DetailListComponent} from './detail-list/detail-list.component';

const routes: Routes = [{
  path: '',
  component: DetailListComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailRoutingModule {
}
