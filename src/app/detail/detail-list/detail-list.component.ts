import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-detail-list',
  templateUrl: './detail-list.component.html',
  styleUrls: ['./detail-list.component.scss']
})
export class DetailListComponent implements OnInit {
  image: String;

  constructor(private apiService: ApiService, private router: Router) {
  }

  ngOnInit() {
    if (this.apiService.image) {
      this.image = this.apiService.image;
    } else {
      this.router.navigate(['/todos']);
    }
  }

}
