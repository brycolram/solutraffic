import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomeRoutingModule} from './home-routing.module';
import {HomeListComponent} from './home-list/home-list.component';
import {NgxLoadingModule} from 'ngx-loading';

@NgModule({
  declarations: [HomeListComponent],
  imports: [
    CommonModule,
    NgxLoadingModule.forRoot({}),
    HomeRoutingModule
  ]
})
export class HomeModule {
}
