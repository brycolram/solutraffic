import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';


@Component({
  selector: 'app-home-list',
  templateUrl: './home-list.component.html',
  styleUrls: ['./home-list.component.scss']
})
export class HomeListComponent implements OnInit {
  image1: any;
  image2: any;
  loading = false;

  constructor(private apiService: ApiService) {

  }

  ngOnInit() {
    this.getImages();
  }

  getImages() {
    this.loading = true;
    this.apiService.getImage().subscribe((data: any) => {
        this.image1 = {
          status: data['status'],
          message: data['message']
        };
        this.loading = false;
      }
    );
    this.apiService.getImage().subscribe((data: any) => {
      this.image2 = {
        status: data['status'],
        message: data['message']
      };
      this.loading = false;
    });
  }
}
