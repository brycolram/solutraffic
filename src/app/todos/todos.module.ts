import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TodosRoutingModule} from './todos-routing.module';
import {TodosListComponent} from './todos-list/todos-list.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgxLoadingModule} from 'ngx-loading';
@NgModule({
  declarations: [TodosListComponent],
  imports: [
    CommonModule,
    NgxPaginationModule,
    NgxLoadingModule.forRoot({}),
    TodosRoutingModule
  ]
})
export class TodosModule {
}
