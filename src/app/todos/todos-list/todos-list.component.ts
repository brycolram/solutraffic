import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-todos-list',
  templateUrl: './todos-list.component.html',
  styleUrls: ['./todos-list.component.scss']
})
export class TodosListComponent implements OnInit {
  allDogs: [];
  p = 1;
  loading = false;

  constructor(private apiService: ApiService, private router: Router) {
  }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.loading = true;
    this.apiService.getAll().subscribe((data: any) => {
      this.allDogs = data['message'];
      this.loading = false;
    });
  }

  detailClick(image: String) {
    this.apiService.image = image;
    this.router.navigate(['/detail']);
  }
}
